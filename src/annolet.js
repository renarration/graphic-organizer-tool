window.onload = function() {
    annoletContainer();
    addEvents();
};

/* function to create container for annolet. This is the user interface through, which the users  
annotate the selected text. */
function annoletContainer() {
    //appending a div(annolet container) to body element of a webpage.
    var body = document.getElementsByTagName("body")[0];
    var container = document.createElement("div");
    container.id = "annolet-container";
    body.appendChild(container);

    //appending a CSS stylesheet into head element of a webpage, which is used to stylize the annolet container.
    var head = document.getElementsByTagName("head")[0];
    var linktag = document.createElement("link");
    linktag.rel = "stylesheet";
    linktag.type = "text/css";
    linktag.href = "https://gl.githack.com/renarration/graphic-organizer-tool/raw/master/src/css/annolet.css"; 
    head.appendChild(linktag);

    // appending jquery script into the body element of a webpage.
  	var jquery_script = document.createElement("script");    
  	jquery_script.type = "text/javascript";
  	jquery_script.src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js";
  	body.appendChild(jquery_script);

    //injecting annolet user interface html code. 
    document.getElementById('annolet-container').innerHTML = "<h3 id='annolet-header'>Webpage-Reassembly</h3>"+
    "<ul id='annolet-tools-menu' >"+
        "<li id='choices' class='menu-list'>"+
        	"<label>Choices</label>"+"<br>"+
        	"<select class='drop_down' id='choices_dropdown' ></select>"+
        "</li>"+
        "<li id='Keywords' class='menu-list'>"+
        	"<label>Keywords</label>"+"<br>"+
        	"<select class='drop_down' id='keywords_dropdown'></select>"+
        "</li>"+
        "<li id='graphical-templates' class='menu-list'>"+
        	"<label>Graphical Templates</label>"+"<br>"+
        	"<select class='drop_down' id='templates_dropdown'></select>"+
        "</li>"+
        "<li class='menu-list'>"+
        	"<div>"+
               "<button id='tag-btn' >Tag it!</button>"+"<br>"+
            "</div>"+
        "</li>"+
        "<li class='menu-list'>"+
        	"<div>"+
               "<button id='graphical-view' >Graphical View</button>"+"<br>"+
            "</div>"+
        "</li>"+
    "</ul>";

    /* This function when called gets the json file (which has the choices, keywords and templates info) from the githack 
    and updates the menu lists of the annolet. */
    getJsondata()
}

function getJsondata()
{
    var url = "https://gl.githack.com/renarration/graphic-organizer-tool/raw/master/src/json/metadata.json"; 
    $.get(url, function(data, status) {
        json_data = data;
        createMenulist(json_data);
    });
}

// using the json data updates the lists based on the default initial choice.
function createMenulist(result){
      for( var choices_key in result.choices){
         	$('#choices_dropdown').append('<option value='+result.choices[choices_key]+'>'+result.choices[choices_key]+'</option>');
      }

      for( var keywords_key in result.keywords){
      		for( var keywords_list in result.keywords[keywords_key] ){
      			$('#keywords_dropdown').append('<option value='+result.keywords[keywords_key][keywords_list]+'>'+result.keywords[keywords_key][keywords_list]+'</option>');
      		}
      		break;
  	  }

    	for( var templates_key in result.templates){
          $('#templates_dropdown').append('<option value='+templates_key+'>'+templates_key+'</option>');
         	break;
      }

      // based on the choice selected updates the keywords and templates list from the json data. 
      updateLists();

      function updateLists() {
  		    document.getElementById("choices_dropdown").addEventListener("change", function(){
          			document.getElementById("keywords_dropdown").options.length = 0;
          			document.getElementById("templates_dropdown").options.length = 0;		
          			var selected_choice = this.value;
          			for( var keywords_list in result.keywords[selected_choice] ){
          				$('#keywords_dropdown').append('<option value='+result.keywords[selected_choice][keywords_list]+'>'+result.keywords[selected_choice][keywords_list]+'</option>');
          			}
          			for( var template in result.templates ){
          				if( selected_choice == template) {
          					$('#templates_dropdown').append('<option value='+template+'>'+template+'</option>');
          				}
        	     }	
  		    });
	    } 

      // adds click eventlistner to the "tag it" button.
      document.getElementById("tag-btn").addEventListener("click", function(){
          annotateText()  
      });
}

// function when called markups the user selected text with the user selected tag from the keywords list.
function annotateText() {
	var selected_tag = document.getElementById("keywords_dropdown").value;
    if(window.getSelection) {
        var text_to_annotate = window.getSelection();
        markupTag(selected_tag, text_to_annotate)
    }
    else if(document.selection && document.selection.type != "Control") {
        var text_to_annotate = document.selection.createRange().text;
        markupTag(selected_tag, text_to_annotate)
    } 
}

function markupTag(cus_tag, str){
    var custom_tag = document.createElement(cus_tag);
    custom_tag.style.backgroundColor = "lightgreen";
    custom_tag.className = "annotated_elem";
    str.getRangeAt(0).surroundContents(custom_tag);     
} 

function addEvents() {
    document.getElementById("graphical-view").addEventListener("click", function(){
      graphicalView()   
    }); 
}

function graphicalView() {

  var strength_analysis = [];
  var weakness_analysis = [];
  var opportunity_analysis = [];
  var threat_analysis = [];

  var annotated_elem_cls = document.getElementsByClassName("annotated_elem");
  for(var i = 0; i < annotated_elem_cls.length;  i++ ) {
      var list_item = document.createElement("li");
      var item_text = document.createTextNode(annotated_elem_cls[i].innerHTML);
      list_item.appendChild(item_text);
      if (annotated_elem_cls[i].tagName == "STRENGTH") {
          strength_analysis.push(list_item);
      }
      else if(annotated_elem_cls[i].tagName == "WEAKNESS"){
          weakness_analysis.push(list_item);
      }
      else if(annotated_elem_cls[i].tagName == "OPPORTUNITY") {
          opportunity_analysis.push(list_item);
      }
      else if(annotated_elem_cls[i].tagName == "THREAT") {
          threat_analysis.push(list_item);
      }
  }
 	
  document.body.style.background= "none";
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", "https://gl.githack.com/renarration/graphic-organizer-tool/raw/master/src/templates/swot_template.html", false);
  xhttp.send();
  document.getElementsByTagName("body")[0].innerHTML = xhttp.responseText;

  for( var i = 0; i < strength_analysis.length; i++ ) {
  	  document.getElementById("strength_analysis").appendChild(strength_analysis[i]);
  } 
  for( var i = 0; i < weakness_analysis.length; i++ ) {
      document.getElementById("weakness_analysis").appendChild(weakness_analysis[i]);
  } 
  for( var i = 0; i < opportunity_analysis.length; i++ ) {
      document.getElementById("opportunity_analysis").appendChild(opportunity_analysis[i]);
  }
  for( var i = 0; i < threat_analysis.length; i++ ) {
      document.getElementById("threat_analysis").appendChild(threat_analysis[i]);
  }
}









